# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

servers=[
  {
      :hostname => "lb",
      :ip => "192.168.33.254",
      :box => "generic/debian10",
      :ram => "256",
      :cpu => "1"
  },
  {
      :hostname => "db",
      :ip => "192.168.33.20",
      :box => "generic/debian10",
      :ram => "512",
      :cpu => "1"
  },
  {
      :hostname => "wp2",
      :ip => "192.168.33.11",
      :box => "generic/debian10",
      :ram => "256",
      :cpu => "1"
  },
  {
      :hostname => "wp1",
      :ip => "192.168.33.10",
      :box => "generic/debian10",
      :ram => "256",
      :cpu => "1"
  },
]

Vagrant.configure("2") do |config|
  servers.each do |machine|
    config.vm.define machine[:hostname] do |node|
      node.vm.box = machine[:box]
      node.vm.hostname = machine[:hostname]
      node.vm.network "private_network", ip: machine[:ip], hostname: true
      node.vm.provision "shell", inline: <<-SHELL
         export DB_HOST=192.168.33.20
         export LB_HOST=192.168.33.254
         export WP1_HOST=192.168.33.10
         export WP2_HOST=192.168.33.11

         #apt-get update
         apt-get install -y screen lynx wget sshpass
         ssh-keygen -b 4096 -t rsa -N "" -q -f ~/.ssh/id_rsa
         echo -e 'password\npassword'  | passwd root
       SHELL
      node.vm.provider "virtualbox" do |vb|
        vb.memory = machine[:ram]
        vb.cpus = machine[:cpu]
        #vb.customize ["modifyvm", :id, "--memory", machine[:ram]]
      end
    end
  end
config.vm.define "lb" do |lb|
    lb.vm.provision "shell", inline: <<-SHELL
         export DB_HOST=192.168.33.20
         export LB_HOST=192.168.33.254
         export WP1_HOST=192.168.33.10
         export WP2_HOST=192.168.33.11

         apt-get install -y apache2
         a2enmod proxy proxy_http proxy_balancer lbmethod_byrequests
         a2dissite 000-default

         cat << EOF > /etc/apache2/sites-available/10-wp-lb.conf
<Proxy "balancer://wpcluster">
  BalancerMember http://$WP1_HOST loadfactor=1
  BalancerMember http://$WP2_HOST loadfactor=1
</Proxy>
<VirtualHost *:80>
  ServerName $LB_HOST
  ProxyPreserveHost On

  # FIXME: could not be working all the time
  # All management stuff will be handled by node1
  ProxyPassMatch "^(wp-admin/|wp-login.php|wp-cron.php)" http://$WP1_HOST/$1

  ProxyPass /cgi-bin http://$WP1_HOST/cgi-bin
  ProxyPassReverse /cgi-bin http://$WP1_HOST/cgi-bin


  ProxyPass / balancer://wpcluster/
</VirtualHost>
EOF

         a2ensite 10-wp-lb.conf

         systemctl reload apache2 && echo 'Apache2 reloaded'
     SHELL
  end

  config.vm.define "db" do |db|
    db.vm.provision "shell", inline: <<-SHELL
         apt-get install -y mariadb-server

         sed -i -e '/bind-address/ s/^#*/#/' /etc/mysql/mariadb.conf.d/50-server.cnf

         MYSQL_ROOT_PASSWORD=password

         mysql -u root -e "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('$MYSQL_ROOT_PASSWORD')"
         mysql -u root -ppassword -e "DROP DATABASE IF EXISTS wordpress"
         mysql -u root -ppassword -e "CREATE DATABASE wordpress"
         mysql -u root -ppassword -e "DROP USER IF EXISTS 'wordpress'@'%'"
         mysql -u root -ppassword -e "CREATE USER 'wordpress'@'%' IDENTIFIED BY 'password'"
         mysql -u root -ppassword -e "GRANT ALL PRIVILEGES ON *.* TO 'wordpress'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION; FLUSH PRIVILEGES;"
         mysql -u root -ppassword -e "FLUSH PRIVILEGES"

         systemctl restart mariadb

     SHELL
  end

  config.vm.define "wp1" do |wp1|
    wp1.vm.provision "file", source: "./wpbalance.zip", destination: "/var/tmp/wpbalance.zip"
    wp1.vm.provision "shell", inline: <<-SHELL
         DB_HOST=192.168.33.20
         WP1_HOST=192.168.33.10
         WP2_HOST=192.168.33.11

         apt-get install -y apache2 php php-curl php-xml php-json  php-mysqli php-mbstring php-zip imagemagick mariadb-client unison
         a2enmod rewrite cgi

         curl -s https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/local/bin/wp
         chmod +x /usr/local/bin/wp

         adminer_ver=4.7.7
         curl -s "https://github.com/vrana/adminer/releases/download/v$adminer_ver/adminer-$adminer_ver-en.php" -o /var/www/html/adminer.php

         cd /var/www/html/
         chown -R www-data:  /var/www/
         { rm /var/www/html/index.html | true; }

         # CGI to run unison via WP
         mkdir -p /var/www/cgi-bin
         cat << EOF > /var/www/cgi-bin/syncwp.cgi
#!/usr/bin/bash
echo "Status: 200"
echo
HOME= /usr/bin/unison 2>&1 -auto -silent -batch -sshargs "-o StrictHostKeyChecking=no" -owner -group /var/www ssh://root@$WP2_HOST//var/www >/dev/null
exit 0
EOF
         chmod u+s,+x /var/www/cgi-bin/syncwp.cgi

         # Enable cgi in apache
         cat << EOF > /etc/apache2/sites-available/10-enable-cgi.conf
  <IfModule mod_cgi.c>
    AddHandler cgi-script .cgi
    <Directory "/var/www/cgi-bin">
      Options +ExecCGI
    </Directory>
    ScriptAlias "/cgi-bin/" "/var/www/cgi-bin/"
  </IfModule>
EOF

         sed -i '/<VirtualHost/r /etc/apache2/sites-available/10-enable-cgi.conf' /etc/apache2/sites-enabled/000-default.conf

         systemctl reload apache2 && echo 'Apache2 reloaded'

         sudo -u www-data wp core download --force
         sudo -u www-data wp config create --dbhost=$DB_HOST --dbuser=wordpress --dbpass=password --dbname=wordpress --force
         sudo -u www-data wp core install --url=$WP1_HOST --title="WP Balance" --admin_user=user --admin_password=password --admin_email=root@localhost.lan

         # Remove useless plugin and install faker press to generate stuff
         sudo -u www-data wp plugin --deactivate uninstall akismet hello
         sudo -u www-data wp plugin install --activate fakerpress
         sudo -u www-data wp plugin install --activate /var/tmp/wpbalance.zip

         sudo -u www-data wp option update uploads_use_yearmonth_folders 0

         # Disable WP cronjob
         sudo -u www-data wp config set WP_DISABLE_CRON true --raw


         sudo -u www-data mkdir -p /var/www/.ssh
         sudo -u www-data ssh-keygen -b 4096 -t rsa -N "" -q -f /var/www/.ssh/id_rsa
         sudo -u www-data sshpass -p password ssh-copy-id -o StrictHostKeyChecking=no root@$WP2_HOST

         sshpass -p password ssh-copy-id -o StrictHostKeyChecking=no root@$WP2_HOST
        

         # Install crontab to sync wp 
         crontab -l | { cat; echo "* * * * * /usr/bin/unison -auto -silent -batch -sshargs '-o StrictHostKeyChecking=no' -times -owner -group /var/www ssh://root@$WP2_HOST//var/www"; } | crontab -
         crontab -l | { cat; echo "* * * * * curl --silent http://$WP1_HOST/wp-cron.php?doing_cron"; } | crontab -

       SHELL
  end

  config.vm.define "wp2" do |wp2|
    wp2.vm.provision "shell", inline: <<-SHELL

         apt-get install -y apache2 php php-curl php-xml php-json  php-mysqli php-mbstring php-zip imagemagick mariadb-client unison

         a2enmod rewrite cgi
         cat << EOF > /etc/apache2/sites-available/10-enable-cgi.conf
  <IfModule mod_cgi.c>
    AddHandler cgi-script .cgi
    <Directory "/var/www/cgi-bin">
      Options +ExecCGI
    </Directory>
    ScriptAlias "/cgi-bin/" "/var/www/cgi-bin/"
  </IfModule>
EOF

         sed -i '/<VirtualHost/r /etc/apache2/sites-available/10-enable-cgi.conf' /etc/apache2/sites-enabled/000-default.conf

         # Reset www. Will be synched from node1 
         rm -rf /var/www/*

         curl -s https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/local/bin/wp
         chmod +x /usr/local/bin/wp

         systemctl reload apache2 && echo 'Apache2 reloaded'

       SHELL
  end

  # config.vm.define "nfs" do |nfs|
    # nfs.vm.provision "shell", inline: <<-SHELL
         # apt-get install -y nfs-server
         # mkdir -p /srv/nfs4/wordpress/wpcontent
         # echo "/srv/nfs4/wordpress/wpcontent  *(rw,sync,no_subtree_check)" >> /etc/exports
         # exportfs -var
     # SHELL
  # end

end
